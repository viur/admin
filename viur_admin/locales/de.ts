< ? xml version = "1.0"
encoding = "utf-8" ?
>
<!DOCTYPE
TS >
<TS version = "2.0"
language = "de_DE" >
<context>
    <name>Accountmanager < / name >
<message>
    <location filename = "accountmanager.py"
line = "60" / >
<source>New < / source >
<translation>Neu < / translation >
< / message >
<message>
    <location filename = "accountmanager.py"
line = "72" / >
<source>Account
deletion < / source >
<translation>Account
löschen < / translation >
< / message >
<message>
    <location filename = "accountmanager.py"
line = "72" / >
<source>Really
delete the
account & quot;
%
s & quot;
? < / source >
<translation>Den Account & quot;
%
s & quot;
wirklich
löschen ? < / translation >
< / message >
< / context >
<context>
    <name>Analytics < / name >
<message>
    <location filename = "analytics.py" line = "124" / >
<source> % s(total)
: %
s < / source >
<translation> % s(gesamt)
: %
s < / translation >
< / message >
<message>
    <location filename = "analytics.py"
line = "126" / >
<source> % s
via % s
: %
s < / source >
<translation> % s
über % s
: %
s < / translation >
< / message >
<message>
    <location filename = "analytics.py"
line = "171" / >
<source> % d. % m. % Y < / source >
<translation> % d. % m. % Y < / translation >
< / message >
< / context >
<context>
    <name>BasePreview < / name >
<message>
    <location filename = "editpreviewUI.py"
line = "47" / >
<source>Reload < / source >
<translation>Neu
laden < / translation >
< / message >
< / context >
<context>
    <name>BooleanEditBone < / name >
<message>
    <location filename = "boolean.py"
line = "25" / >
<source>Yes < / source >
<translation>Ja < / translation >
< / message >
<message>
    <location filename = "boolean.py"
line = "25" / >
<source>No < / source >
<translation>Nein < / translation >
< / message >
< / context >
<context>
    <name>CalenderList < / name >
<message>
    <location filename = "calender.py"
line = "12" / >
<source>unfiltered < / source >
<translation>ungefiltert < / translation >
< / message >
<message>
    <location filename = "calender.py"
line = "13" / >
<source>Year < / source >
<translation>Jahr < / translation >
< / message >
<message>
    <location filename = "calender.py"
line = "14" / >
<source>Month < / source >
<translation>Monat < / translation >
< / message >
<message>
    <location filename = "calender.py"
line = "15" / >
<source>Day < / source >
<translation>Tag < / translation >
< / message >
<message>
    <location filename = "calender.py"
line = "37" / >
<source>yyyy < / source >
<translation>yyyy < / translation >
< / message >
<message>
    <location filename = "calender.py"
line = "40" / >
<source>MM.yyyy < / source >
<translation>MM.yyyy < / translation >
< / message >
<message>
    <location filename = "calender.py"
line = "43" / >
<source>dd.MM.yyyy < / source >
<translation>dd.MM.yyyy < / translation >
< / message >
< / context >
<context>
    <name>DialogCreateTable < / name >
<message>
    <location filename = "createtableUI.py"
line = "66" / >
<source>Create
Table < / source >
<translation>Tabelle
erstellen < / translation >
< / message >
<message>
    <location filename = "createtableUI.py"
line = "67" / >
<source>Rows < / source >
<translation>Reihen < / translation >
< / message >
<message>
    <location filename = "createtableUI.py"
line = "68" / >
<source>Columns < / source >
<translation>Spalten < / translation >
< / message >
<message>
    <location filename = "createtableUI.py"
line = "69" / >
<source>Alignment < / source >
<translation>Ausrichtung < / translation >
< / message >
<message>
    <location filename = "createtableUI.py"
line = "70" / >
<source>Left < / source >
<translation>Links < / translation >
< / message >
<message>
    <location filename = "createtableUI.py"
line = "71" / >
<source>Center < / source >
<translation>Zentriert < / translation >
< / message >
<message>
    <location filename = "createtableUI.py"
line = "72" / >
<source>Right < / source >
<translation>Rechts < / translation >
< / message >
< / context >
<context>
    <name>DocEdit < / name >
<message>
    <location filename = "docEditUI.py"
line = "115" / >
<source>Form < / source >
<translation>Form < / translation >
< / message >
<message>
    <location filename = "docEditUI.py"
line = "116" / >
<source>Import / Export < / source >
<translation>Import / Export < / translation >
< / message >
<message>
    <location filename = "docEditUI.py"
line = "118" / >
<source>New
caption < / source >
<translation>Neue
Überschrift < / translation >
< / message >
<message>
    <location filename = "docEditUI.py"
line = "119" / >
<source>New
Image < / source >
<translation>Neues
Bild < / translation >
< / message >
<message>
    <location filename = "docEditUI.py"
line = "120" / >
<source>New
Text < / source >
<translation>Neuer
Text < / translation >
< / message >
<message>
    <location filename = "docEditUI.py"
line = "121" / >
<source>Apply < / source >
<translation>Übernehmen < / translation >
< / message >
<message>
    <location filename = "docEditUI.py"
line = "122" / >
<source>New
Table < / source >
<translation>Neue
Tabelle < / translation >
< / message >
<message>
    <location filename = "docEditUI.py"
line = "123" / >
<source>Extensions < / source >
<translation>Erweiterung < / translation >
< / message >
<message>
    <location filename = "docEditUI.py"
line = "124" / >
<source>Add < / source >
<translation>Hinzufügen < / translation >
< / message >
< / context >
<context>
    <name>DocumentEditBone < / name >
<message>
    <location filename = "document.py"
line = "17" / >
<source>Open
editor < / source >
<translation>Editor
öffnen < / translation >
< / message >
<message>
    <location filename = "docEdit.py"
line = "71" / >
<source>Edit
link < / source >
<translation>Link
bearbeiten < / translation >
< / message >
<message>
    <location filename = "docEdit.py"
line = "769" / >
<source>Table < / source >
<translation>Tabelle < / translation >
< / message >
<message>
    <location filename = "docEdit.py"
line = "141" / >
<source>Insert < / source >
<translation>Einfügen < / translation >
< / message >
<message>
    <location filename = "docEdit.py"
line = "142" / >
<source>Row
before < / source >
<translation>Reihe
vor < / translation >
< / message >
<message>
    <location filename = "docEdit.py"
line = "143" / >
<source>Row
after < / source >
<translation>Reihe
zurück < / translation >
< / message >
<message>
    <location filename = "docEdit.py"
line = "144" / >
<source>Column
before < / source >
<translation>Spalte
vor < / translation >
< / message >
<message>
    <location filename = "docEdit.py"
line = "145" / >
<source>Column
after < / source >
<translation>Spalte
zurück < / translation >
< / message >
<message>
    <location filename = "docEdit.py"
line = "146" / >
<source>Remove < / source >
<translation>Rückgängig < / translation >
< / message >
<message>
    <location filename = "docEdit.py"
line = "147" / >
<source>This
row < / source >
<translation>Diese
Reihe < / translation >
< / message >
<message>
    <location filename = "docEdit.py"
line = "148" / >
<source>This
column < / source >
<translation>Diese
Spalte < / translation >
< / message >
<message>
    <location filename = "docEdit.py"
line = "1522" / >
<source>Document
editor < / source >
<translation>Dokument - Editor < / translation >
< / message >
<message>
    <location filename = "docEdit.py"
line = "799" / >
<source>Text < / source >
<translation>Text < / translation >
< / message >
<message>
    <location filename = "docEdit.py"
line = "832" / >
<source>Image < / source >
<translation>Bild < / translation >
< / message >
<message>
    <location filename = "docEdit.py"
line = "863" / >
<source>(empty
caption
)
< / source >
<translation>(keine
Überschrift
)
< / translation >
< / message >
<message>
    <location filename = "docEdit.py"
line = "882" / >
<source>Change
caption < / source >
<translation>Überschrift
ändern < / translation >
< / message >
<message>
    <location filename = "docEdit.py"
line = "1480" / >
<source>Insert
new caption < / source >
<translation>Neue
Überschrift
einfügen < / translation >
< / message >
<message>
    <location filename = "docEdit.py"
line = "1480" / >
<source>New
caption < / source >
<translation>Neue
Überschrift < / translation >
< / message >
<message>
    <location filename = "docEdit.py"
line = "1488" / >
<source>New
text < / source >
<translation>Neuer
Text < / translation >
< / message >
< / context >
<context>
    <name>Edit < / name >
<message>
    <location filename = "editUI.py"
line = "84" / >
<source>Form < / source >
<translation>Form < / translation >
< / message >
<message>
    <location filename = "editUI.py"
line = "85" / >
<source>Reset < / source >
<translation>Zurücksetzen < / translation >
< / message >
<message>
    <location filename = "editUI.py"
line = "87" / >
<source>Preview < / source >
<translation>Vorschau < / translation >
< / message >
<message>
    <location filename = "editUI.py"
line = "89" / >
<source>Save
and
continue
< / source >
<translation>Speichern
und
Weiter < / translation >
< / message >
<message>
    <location filename = "editUI.py"
line = "88" / >
<source>Save
and
close < / source >
<translation>Speichern
und
Schließen < / translation >
< / message >
<message>
    <location filename = "editUI.py"
line = "86" / >
<source>Close < / source >
<translation>Schließen < / translation >
< / message >
< / context >
<context>
    <name>EditWidget < / name >
<message>
    <location filename = "edit.py"
line = "103" / >
<source>Execute < / source >
<translation>Ausführen < / translation >
< / message >
<message>
    <location filename = "edit.py"
line = "115" / >
<source>Save
and
Close < / source >
<translation>Speichern
und
Schließen < / translation >
< / message >
<message>
    <location filename = "edit.py"
line = "113" / >
<source>Save
and
New < / source >
<translation>Speichern
und
Neu < / translation >
< / message >
<message>
    <location filename = "edit.py"
line = "116" / >
<source>Save
and
Continue < / source >
<translation>Speichern
und
weiter < / translation >
< / message >
<message>
    <location filename = "edit.py"
line = "141" / >
<source>Clone
: %
s < / source >
<translation>Klonen
: %
s < / translation >
< / message >
<message>
    <location filename = "edit.py"
line = "143" / >
<source>Clone
entry < / source >
<translation>Eintrag
klonen < / translation >
< / message >
<message>
    <location filename = "edit.py"
line = "147" / >
<source>Edit
: %
s < / source >
<translation>Bearbeiten
: %
s < / translation >
< / message >
<message>
    <location filename = "edit.py"
line = "149" / >
<source>Edit
entry < / source >
<translation>Eintrag
bearbeiten < / translation >
< / message >
<message>
    <location filename = "edit.py"
line = "152" / >
<source>Add
entry < / source >
<translation>Eintrag
hinzufügen < / translation >
< / message >
<message>
    <location filename = "edit.py"
line = "192" / >
<source>Confirm
reset < / source >
<translation>Zurücksetzen
bestätigen < / translation >
< / message >
<message>
    <location filename = "edit.py"
line = "192" / >
<source>Discard
all
unsaved
changes ? < / source >
<translation>Alle ungespeicherten
Änderungen
verwerfen ? < / translation >
< / message >
<message>
    <location filename = "edit.py" line = "259" / >
<source>General < / source >
<translation>Allgemeines < / translation >
< / message >
<message>
    <location filename = "edit.py"
line = "351" / >
<source>Entry
saved < / source >
<translation>Eintrag
gespeichert < / translation >
< / message >
<message>
    <location filename = "edit.py"
line = "365" / >
<source>Missing
data < / source >
<translation>Fehlende
Daten < / translation >
< / message >
<message>
    <location filename = "edit.py"
line = "372" / >
<source>There
was
an
error
saving
your
changes < / source >
<translation>Es
gab
einen
Fehler
beim
Speichern
der
Änderungen < / translation >
< / message >
<message>
    <location filename = "edit.py"
line = "377" / >
<source>Task
created < / source >
<translation>Aufgabe
erstellt < / translation >
< / message >
<message>
    <location filename = "edit.py"
line = "377" / >
<source>The
task
was
sucessfully
created. < / source >
<translation>Die
Aufgabe
wurde
erfolgreich
erstellt. < / translation >
< / message >
<message>
    <location filename = "edit.py"
line = "377" / >
<source>Okay < / source >
<translation>Ok < / translation >
< / message >
< / context >
<context>
    <name>FileDownloadProgress < / name >
<message>
    <location filename = "fileDownloadProgressUI.py"
line = "51" / >
<source>Form < / source >
<translation>Form < / translation >
< / message >
<message>
    <location filename = "fileDownloadProgressUI.py"
line = "52" / >
<source>ProgressTotal < / source >
<translation>Fortschritt
Gesamt < / translation >
< / message >
<message>
    <location filename = "fileDownloadProgressUI.py"
line = "53" / >
<source>Progress < / source >
<translation>Fortschritt < / translation >
< / message >
<message>
    <location filename = "fileDownloadProgressUI.py"
line = "54" / >
<source>Cancel < / source >
<translation>Abbrechen < / translation >
< / message >
< / context >
<context>
    <name>FileHandler < / name >
<message>
    <location filename = "file.py"
line = "131" / >
<source>Files
: %
s / % s, Directories
: %
s / % s, Bytes
: %
s / % s < / source >
<translation>Dateien
: %
s
von % s, Verzeichnisse
: %
s
von % s, Bytes
: %
s
von % s < / translation >
< / message >
<message>
    <location filename = "file.py"
line = "18" / >
<source>Upload
files < / source >
<translation>Dateien
hochladen < / translation >
< / message >
<message>
    <location filename = "file.py"
line = "35" / >
<source>Download
files < / source >
<translation>Dateien
herunterladen < / translation >
< / message >
< / context >
<context>
    <name>FileUploadProgress < / name >
<message>
    <location filename = "fileUploadProgressUI.py"
line = "57" / >
<source>Form < / source >
<translation>Form < / translation >
< / message >
<message>
    <location filename = "fileUploadProgressUI.py"
line = "58" / >
<source>ProgressTotal < / source >
<translation>Fortschritt
Gesamt < / translation >
< / message >
<message>
    <location filename = "fileUploadProgressUI.py"
line = "59" / >
<source>ProgressFile < / source >
<translation>Fortschritt
Datei < / translation >
< / message >
<message>
    <location filename = "fileUploadProgressUI.py"
line = "60" / >
<source>Progress < / source >
<translation>Fortschritt < / translation >
< / message >
<message>
    <location filename = "fileUploadProgressUI.py"
line = "61" / >
<source>Cancel < / source >
<translation>Abbrechen < / translation >
< / message >
< / context >
<context>
    <name>Form < / name >
<message>
    <location filename = "docEditfileEditUI.py"
line = "124" / >
<source>Form < / source >
<translation>Form < / translation >
< / message >
<message>
    <location filename = "docEditfileEditUI.py"
line = "125" / >
<source>x < / source >
<translation>x < / translation >
< / message >
<message>
    <location filename = "docEditfileEditUI.py"
line = "126" / >
<source> % < / source >
<translation> % < / translation >
< / message >
<message>
    <location filename = "docEditfileEditUI.py"
line = "127" / >
<source>px < / source >
<translation>px < / translation >
< / message >
<message>
    <location filename = "docEditfileEditUI.py"
line = "128" / >
<source>em < / source >
<translation>em < / translation >
< / message >
<message utf8 = "true" >
<location filename = "docEditfileEditUI.py"
line = "129" / >
<source>Größe < / source >
<translation>Größe < / translation >
< / message >
<message utf8 = "true" >
<location filename = "docEditfileEditUI.py"
line = "130" / >
<source>Auswählen < / source >
<translation>< / translation >
< / message >
<message>
    <location filename = "docEditfileEditUI.py"
line = "131" / >
<source>Datei < / source >
<translation>< / translation >
< / message >
<message>
    <location filename = "docEditfileEditUI.py"
line = "132" / >
<source>Links < / source >
<translation>< / translation >
< / message >
<message>
    <location filename = "docEditfileEditUI.py"
line = "133" / >
<source>Mitte < / source >
<translation>< / translation >
< / message >
<message>
    <location filename = "docEditfileEditUI.py"
line = "134" / >
<source>Rechts < / source >
<translation>< / translation >
< / message >
<message utf8 = "true" >
<location filename = "docEditfileEditUI.py"
line = "135" / >
<source>FolgendenText
umfließen
lassen < / source >
<translation>< / translation >
< / message >
<message>
    <location filename = "docEditfileEditUI.py"
line = "136" / >
<source>Ausrichtung < / source >
<translation>< / translation >
< / message >
<message>
    <location filename = "docEditfileEditUI.py"
line = "137" / >
<source>Titel < / source >
<translation>< / translation >
< / message >
<message>
    <location filename = "docEditfileEditUI.py"
line = "138" / >
<source>Alternativ - Text < / source >
<translation>< / translation >
< / message >
<message utf8 = "true" >
<location filename = "docEditfileEditUI.py"
line = "139" / >
<source>Verknüpfen
mit < / source >
<translation>< / translation >
< / message >
<message>
    <location filename = "docEditfileEditUI.py"
line = "140" / >
<source>-Keine
Verlinkung - < / source >
<translation>< / translation >
< / message >
<message utf8 = "true" >
<location filename = "docEditfileEditUI.py"
line = "141" / >
<source>Vergrößern < / source >
<translation>< / translation >
< / message >
<message utf8 = "true" >
<location filename = "docEditfileEditUI.py"
line = "142" / >
<source>In
neuem
Fenster
öffnen < / source >
<translation>< / translation >
< / message >
< / context >
<context>
    <name>Help < / name >
<message>
    <location filename = "mainwindow.py"
line = "367" / >
<source>Help < / source >
<translation>Hilfe < / translation >
< / message >
< / context >
<context>
    <name>Hierarchy < / name >
<message>
    <location filename = "hierarchy.py"
line = "363" / >
<source>Edit
entry < / source >
<translation>Eintrag
bearbeiten < / translation >
< / message >
<message>
    <location filename = "hierarchy.py"
line = "25" / >
<source>Add
entry < / source >
<translation>Eintrag
hinzufügen < / translation >
< / message >
<message>
    <location filename = "hierarchy.py"
line = "60" / >
<source>Delete
entry < / source >
<translation>Eintrag
löschen < / translation >
< / message >
<message>
    <location filename = "hierarchyUI.py"
line = "60" / >
<source>Form < / source >
<translation>Form < / translation >
< / message >
< / context >
<context>
    <name>HierarchySelector < / name >
<message>
    <location filename = "hierarchySelectorUI.py"
line = "52" / >
<source>Form < / source >
<translation>Form < / translation >
< / message >
<message>
    <location filename = "hierarchySelectorUI.py"
line = "53" / >
<source>Selected
:
< / source >
<translation>Ausgewählt
:
< / translation >
< / message >
<message>
    <location filename = "hierarchySelectorUI.py"
line = "54" / >
<source>Apply < / source >
<translation>Übernehmen < / translation >
< / message >
< / context >
<context>
    <name>HierarchyTreeWidget < / name >
<message>
    <location filename = "hierarchy.py"
line = "268" / >
<source>Confirm
delete< / source >
<translation>Löschen
bestätigen < / translation >
< / message >
<message>
    <location filename = "hierarchy.py"
line = "268" / >
<source>Delete % s
entries
and
everything
below ? < / source >
<translation> % s Einträge
samt
Untereinträgen
löschen ? < / translation >
< / message >
< / context >
<context>
    <name>LinkEdit < / name >
<message>
    <location filename = "docEditlinkEditUI.py" line = "46" / >
<source>Form < / source >
<translation>Form < / translation >
< / message >
<message utf8 = "true" >
<location filename = "docEditlinkEditUI.py"
line = "47" / >
<source>Verknüpfungsziel < / source >
<translation>< / translation >
< / message >
<message utf8 = "true" >
<location filename = "docEditlinkEditUI.py"
line = "48" / >
<source>In
neuem
Fenster
öffnen < / source >
<translation>< / translation >
< / message >
< / context >
<context>
    <name>List < / name >
<message>
    <location filename = "listUI.py"
line = "63" / >
<source>Form < / source >
<translation>Form < / translation >
< / message >
<message>
    <location filename = "calenderlistUI.py"
line = "77" / >
<source>dd.MM.yyyy < / source >
<translation>tt.mm.jjjj < / translation >
< / message >
<message>
    <location filename = "listUI.py"
line = "64" / >
<source>Search < / source >
<translation>Suchen < / translation >
< / message >
<message>
    <location filename = "list.py"
line = "566" / >
<source>Clone
: %
s < / source >
<translation>Klonen
: %
s < / translation >
< / message >
<message>
    <location filename = "list.py"
line = "568" / >
<source>Clone
entry < / source >
<translation>Eintrag
klonen < / translation >
< / message >
<message>
    <location filename = "list.py"
line = "572" / >
<source>Edit
: %
s < / source >
<translation>Bearbeiten
: %
s < / translation >
< / message >
<message>
    <location filename = "list.py"
line = "574" / >
<source>Edit
entry < / source >
<translation>Eintrag
bearbeiten < / translation >
< / message >
<message>
    <location filename = "list.py"
line = "411" / >
<source>No
items in the
current
selection < / source >
<translation>Die
aktuelle
Auswahl
enthält
keine
Einträge < / translation >
< / message >
<message>
    <location filename = "listUI.py"
line = "65" / >
<source>Prefix
search < / source >
<translation>Schnellsuche < / translation >
< / message >
<message>
    <location filename = "listUI.py"
line = "66" / >
<source>Fulltext
search < / source >
<translation type = "unfinished" > Volltextsuche < / translation >
< / message >
< / context >
<context>
    <name>ListHandler < / name >
<message>
    <location filename = "list.py"
line = "14" / >
<source>Add
entry < / source >
<translation>Eintrag
hinzufügen < / translation >
< / message >
<message>
    <location filename = "list.py"
line = "25" / >
<source>Add
entry: %
s < / source >
<translation>Eintrag
hinzufügen: %
s < / translation >
< / message >
<message>
    <location filename = "list.py"
line = "39" / >
<source>Edit
entry < / source >
<translation>Eintrag
bearbeiten < / translation >
< / message >
<message>
    <location filename = "list.py"
line = "85" / >
<source>Delete < / source >
<translation>Löschen < / translation >
< / message >
<message>
    <location filename = "list.py"
line = "122" / >
<source>Preview
: %
s < / source >
<translation>Vorschau
: %
s < / translation >
< / message >
<message>
    <location filename = "list.py"
line = "159" / >
<source>Preview < / source >
<translation>Vorschau < / translation >
< / message >
<message>
    <location filename = "list.py"
line = "150" / >
<source>Preview
not
possible < / source >
<translation>Vorschau
nicht
möglich < / translation >
< / message >
<message>
    <location filename = "list.py"
line = "66" / >
<source>Clone
entry < / source >
<translation>Eintrag
klonen < / translation >
< / message >
<message>
    <location filename = "list.py"
line = "49" / >
<source>Edit
multiple
Entries < / source >
<translation>Mehrere
Einträge
bearbeiten < / translation >
< / message >
<message>
    <location filename = "list.py"
line = "49" / >
<source>Edit
all % s
accounts ? < / source >
<translation>Alle % s Accounts
bearbeiten ? < / translation >
< / message >
< / context >
<context>
    <name>ListTableView < / name >
<message>
    <location filename = "list.py" line = "344" / >
<source>Confirm
delete< / source >
<translation>Löschen
bestätigen < / translation >
< / message >
<message>
    <location filename = "list.py"
line = "344" / >
<source>Delete % s
entries ? < / source >
<translation> % s Einträge
Löschen ? < / translation >
< / message >
<message>
    <location filename = "list.py" line = "356" / >
<source>Deleting
: %
s
of % s
removed. < / source >
<translation>Lösche
: %
s
von % s
gelöscht < / translation >
< / message >
< / context >
<context>
    <name>Login < / name >
<message>
    <location filename = "login.py"
line = "237" / >
<source>Save
this
account < / source >
<translation>Diesen
Account
speichern < / translation >
< / message >
<message>
    <location filename = "login.py"
line = "232" / >
<source>Save
this
account
permanently ? < / source >
<translation>Diesen Account
dauerhaft
speichern ? < / translation >
< / message >
<message>
    <location filename = "login.py" line = "235" / >
<source>Enter
a
name
for this account < / source >
<translation>Geben
Sie
einen
Namen
für
diesen
Account
ein < / translation >
< / message >
<message>
    <location filename = "login.py"
line = "237" / >
<source>Save
the
password, too ? < / source >
<translation>Das Passwort
ebenfalls
speichern ? < / translation >
< / message >
<message>
    <location filename = "login.py" line = "255" / >
<source>Login in progress < / source >
<translation>Einloggen
im
Gange < / translation >
< / message >
<message>
    <location filename = "login.py"
line = "298" / >
<source>Captcha
required < / source >
<translation>Captcha
notwendig < / translation >
< / message >
<message>
    <location filename = "login.py"
line = "215" / >
<source>Login
successful < / source >
<translation>Login
erfolgreich < / translation >
< / message >
<message>
    <location filename = "login.py"
line = "164" / >
<source>Not
required < / source >
<translation>Nicht
benötigt < / translation >
< / message >
<message>
    <location filename = "login.py"
line = "349" / >
<source>This
Email
address
looks
valid < / source >
<translation>Diese
EMailadresse
scheint
gültig
zu
sein < / translation >
< / message >
<message>
    <location filename = "login.py"
line = "352" / >
<source>This
Email
address
is
invalid < / source >
<translation>Diese
EMailadresse
ist
ungültig < / translation >
< / message >
< / context >
<context>
    <name>LoginWindow < / name >
<message>
    <location filename = "loginformUI.py"
line = "173" / >
<source>Portal < / source >
<translation>Portal < / translation >
< / message >
<message>
    <location filename = "loginformUI.py"
line = "174" / >
<source>Server < / source >
<translation>Server < / translation >
< / message >
<message>
    <location filename = "loginformUI.py"
line = "175" / >
<source>Captcha < / source >
<translation>Captcha < / translation >
< / message >
<message>
    <location filename = "loginformUI.py"
line = "177" / >
<source>Password < / source >
<translation>Passwort < / translation >
< / message >
<message>
    <location filename = "loginformUI.py"
line = "178" / >
<source>Accountmannager < / source >
<translation>Account
Manager < / translation >
< / message >
<message>
    <location filename = "loginformUI.py"
line = "180" / >
<source>Language < / source >
<translation>Sprache < / translation >
< / message >
<message>
    <location filename = "loginformUI.py"
line = "181" / >
<source>Login < / source >
<translation>Einloggen < / translation >
< / message >
<message>
    <location filename = "loginformUI.py"
line = "182" / >
<source>Info < / source >
<translation>Info < / translation >
< / message >
<message>
    <location filename = "loginformUI.py"
line = "184" / >
<source>Erste
Schritte < / source >
<translation>< / translation >
< / message >
<message>
    <location filename = "loginformUI.py"
line = "188" / >
<source>Accountmanager < / source >
<translation>Account
Manager < / translation >
< / message >
<message>
    <location filename = "loginformUI.py"
line = "176" / >
<source>User < / source >
<comment>xy &lt; < / comment >
<translation>Nutzer < / translation >
< / message >
<message>
    <location filename = "loginformUI.py"
line = "183" / >
<source>Settings < / source >
<translation>Einstellungen < / translation >
< / message >
<message>
    <location filename = "loginformUI.py"
line = "187" / >
<source>Generel
settigs < / source >
<translation>Allgemeine
Einstellungen < / translation >
< / message >
<message utf8 = "true" >
<location filename = "loginformUI.py"
line = "172" / >
<source>ViurAdmin
–
Hello
!< / source >
<translation>ViurAdmin
–
Hallo
!< / translation >
< / message >
<message>
    <location filename = "loginformUI.py"
line = "186" / >
<source>About
this
Software < / source >
<translation>Über
diese
Software < / translation >
< / message >
<message>
    <location filename = "loginformUI.py"
line = "185" / >
<source>Help < / source >
<translation>Hilfe < / translation >
< / message >
<message>
    <location filename = "loginformUI.py"
line = "179" / >
<source>Not
required < / source >
<translation>Nicht
benötigt < / translation >
< / message >
< / context >
<context>
    <name>MainWindow < / name >
<message>
    <location filename = "accountmanagerUI.py"
line = "123" / >
<source>Viur
Accountmanager < / source >
<translation>Viur
Accountmanager < / translation >
< / message >
<message>
    <location filename = "accountmanagerUI.py"
line = "126" / >
<source>Account
Name < / source >
<translation>Account
Name < / translation >
< / message >
<message>
    <location filename = "accountmanagerUI.py"
line = "124" / >
<source>New
Account < / source >
<translation>Neuer
Account < / translation >
< / message >
<message>
    <location filename = "accountmanagerUI.py"
line = "125" / >
<source>Delete
Account < / source >
<translation>Account
löschen < / translation >
< / message >
<message>
    <location filename = "accountmanagerUI.py"
line = "127" / >
<source>Serveradress < / source >
<translation>Serveradresse < / translation >
< / message >
<message>
    <location filename = "accountmanagerUI.py"
line = "128" / >
<source>Username < / source >
<translation>Benutzer < / translation >
< / message >
<message>
    <location filename = "accountmanagerUI.py"
line = "129" / >
<source>Userpassword < / source >
<translation>Passwort < / translation >
< / message >
<message>
    <location filename = "accountmanagerUI.py"
line = "130" / >
<source>Save
Password < / source >
<translation>Passwort
sichern < / translation >
< / message >
<message>
    <location filename = "accountmanagerUI.py"
line = "131" / >
<source>Back
to
Loginscreen < / source >
<translation>Zurück
zum
Loginscreen < / translation >
< / message >
<message>
    <location filename = "mainwindow.py"
line = "280" / >
<source>Welcome
to
ViUR
!< / source >
<translation>Willkommen
bei
ViUR
!< / translation >
< / message >
<message>
    <location filename = "adminUI.py"
line = "128" / >
<source>ViUR
Admin < / source >
<translation>ViUR
Admin < / translation >
< / message >
<message>
    <location filename = "adminUI.py"
line = "130" / >
<source>Module < / source >
<translation>Module < / translation >
< / message >
<message>
    <location filename = "adminUI.py"
line = "132" / >
<source>TextLabel < / source >
<translation>Beschriftung < / translation >
< / message >
<message>
    <location filename = "adminUI.py"
line = "133" / >
<source>Info < / source >
<translation>Hinweis < / translation >
< / message >
<message>
    <location filename = "adminUI.py"
line = "134" / >
<source>Advanced < / source >
<translation>Erweitert < / translation >
< / message >
<message>
    <location filename = "adminUI.py"
line = "135" / >
<source>Beenden < / source >
<translation>< / translation >
< / message >
<message>
    <location filename = "adminUI.py"
line = "136" / >
<source>Erste
Schritte < / source >
<translation>< / translation >
< / message >
<message>
    <location filename = "adminUI.py"
line = "137" / >
<source>Help < / source >
<translation>Hilfe < / translation >
< / message >
<message>
    <location filename = "adminUI.py"
line = "138" / >
<source>About < / source >
<translation>Über < / translation >
< / message >
<message>
    <location filename = "adminUI.py"
line = "139" / >
<source>Ausloggen < / source >
<translation>< / translation >
< / message >
<message>
    <location filename = "adminUI.py"
line = "140" / >
<source>Tasks < / source >
<translation>Aufgaben < / translation >
< / message >
< / context >
<context>
    <name>NetworkService < / name >
<message>
    <location filename = "network.py"
line = "457" / >
<source>Insecure
connection < / source >
<translation>Unverschlüsselte
Verbindung < / translation >
< / message >
<message>
    <location filename = "network.py"
line = "457" / >
<source>Continue in unsecure
mode < / source >
<translation>Trotz
unverschlüsselte
Verbindung
fortfahren < / translation >
< / message >
<message>
    <location filename = "network.py"
line = "457" / >
<source>Abort < / source >
<translation>Abbrechen < / translation >
< / message >
< / context >
<context>
    <name>OrderHandler < / name >
<message>
    <location filename = "list_order.py"
line = "19" / >
<source>Payment
recived < / source >
<translation>Zahlung
erhalten < / translation >
< / message >
<message>
    <location filename = "list_order.py"
line = "30" / >
<source>Mark
as
payed < / source >
<translation>Als
bezahlt
markieren < / translation >
< / message >
<message>
    <location filename = "list_order.py"
line = "30" / >
<source>Mark % s
orders
as
payed ? < / source >
<translation> % s Bestellungen
als
bezahlt
markieren ? < / translation >
< / message >
<message>
    <location filename = "list_order.py" line = "45" / >
<source>Order
Shipped < / source >
<translation>Bestellung
verschickt < / translation >
< / message >
<message>
    <location filename = "list_order.py"
line = "88" / >
<source>Mark
shipped < / source >
<translation>Als
versendet
markieren < / translation >
< / message >
<message>
    <location filename = "list_order.py"
line = "55" / >
<source>Mark % s
orders
as
shipped ? < / source >
<translation> % s Bestellungen
als
versendet
markieren ? < / translation >
< / message >
<message>
    <location filename = "list_order.py" line = "9" / >
<source>Please
confirm < / source >
<translation>Bitte
bestätigen < / translation >
< / message >
<message>
    <location filename = "list_order.py"
line = "78" / >
<source>Order
canceled < / source >
<translation>Bestellung
storniert < / translation >
< / message >
<message>
    <location filename = "list_order.py"
line = "88" / >
<source>Cancel % s
orders ? < / source >
<translation> % s Bestellungen
stornieren ? < / translation >
< / message >
<message>
    <location filename = "list_order.py" line = "131" / >
<source>Bill < / source >
<translation>Rechnung < / translation >
< / message >
<message>
    <location filename = "list_order.py"
line = "167" / >
<source>Delivery
Note < / source >
<translation>Lieferschein < / translation >
< / message >
<message>
    <location filename = "list_order.py"
line = "111" / >
<source>Download
Bill < / source >
<translation>Rechnung
herunterladen < / translation >
< / message >
<message>
    <location filename = "list_order.py"
line = "146" / >
<source>Download
delivery
note < / source >
<translation>Lieferschein
herunterladen < / translation >
< / message >
< / context >
<context>
    <name>Preloader < / name >
<message>
    <location filename = "preloaderUI.py"
line = "47" / >
<source>Loading
...
< / source >
<translation>Lade
...
< / translation >
< / message >
< / context >
<context>
    <name>Preview < / name >
<message>
    <location filename = "edit.py"
line = "35" / >
<source>Preview
not
possible < / source >
<translation>Vorschau
nicht
möglich < / translation >
< / message >
< / context >
<context>
    <name>RelationalBoneSelector < / name >
<message>
    <location filename = "relational.py"
line = "271" / >
<source>Select % s < / source >
<translation>Wähle % s < / translation >
< / message >
< / context >
<context>
    <name>RelationalEditBone < / name >
<message>
    <location filename = "relational.py"
line = "92" / >
<source>Change
selection < / source >
<translation>Auswahl
ändern < / translation >
< / message >
< / context >
<context>
    <name>Task < / name >
<message>
    <location filename = "taskUI.py"
line = "54" / >
<source>Form < / source >
<translation>Form < / translation >
< / message >
<message>
    <location filename = "taskUI.py"
line = "55" / >
<source>TextLabel < / source >
<translation>Beschriftung < / translation >
< / message >
<message>
    <location filename = "taskUI.py"
line = "56" / >
<source>Execute < / source >
<translation>Ausführen < / translation >
< / message >
< / context >
<context>
    <name>TextEdit < / name >
<message>
    <location filename = "text.py"
line = "403" / >
<source>Apply < / source >
<translation>Übernehmen < / translation >
< / message >
< / context >
<context>
    <name>TextEditBone < / name >
<message>
    <location filename = "text.py"
line = "1030" / >
<source>Open
editor < / source >
<translation>Editor
öffnen < / translation >
< / message >
<message>
    <location filename = "text.py"
line = "500" / >
<source>Text
edit < / source >
<translation>Text
bearbeiten < / translation >
< / message >
<message>
    <location filename = "text.py"
line = "740" / >
<source>Specify
target < / source >
<translation>Ziel
bestimmen < / translation >
< / message >
<message>
    <location filename = "text.py"
line = "740" / >
<source>Link
target:< / source >
<translation>Ziel
verlinken:< / translation >
< / message >
< / context >
<context>
    <name>Tree < / name >
<message>
    <location filename = "treeUI.py"
line = "60" / >
<source>Form < / source >
<translation>Form < / translation >
< / message >
<message>
    <location filename = "treeUI.py"
line = "62" / >
<source>Search < / source >
<translation>Suchen < / translation >
< / message >
< / context >
<context>
    <name>TreeHandler < / name >
<message>
    <location filename = "tree.py"
line = "103" / >
<source>Delete < / source >
<translation>Löschen < / translation >
< / message >
<message>
    <location filename = "tree.py"
line = "23" / >
<source>Add
entry < / source >
<translation>Eintrag
hinzufügen < / translation >
< / message >
<message>
    <location filename = "tree.py"
line = "68" / >
<source>Directory
up < / source >
<translation>Verzeichnis
nach
oben < / translation >
< / message >
<message>
    <location filename = "tree_simple.py"
line = "64" / >
<source>New
directory < / source >
<translation>Neues
Verzeichnis < / translation >
< / message >
<message>
    <location filename = "tree_simple.py"
line = "70" / >
<source>Create
directory < / source >
<translation>Verzeichnis
erstellen < / translation >
< / message >
<message>
    <location filename = "tree_simple.py"
line = "70" / >
<source>Directory
name < / source >
<translation>Verzeichnisname < / translation >
< / message >
<message>
    <location filename = "tree_simple.py"
line = "39" / >
<source>Edit
entry < / source >
<translation>Eintrag
bearbeiten < / translation >
< / message >
<message>
    <location filename = "tree.py"
line = "136" / >
<source>Switch
View < / source >
<translation>Ansicht
wechseln < / translation >
< / message >
<message>
    <location filename = "tree_simple.py"
line = "88" / >
<source>Rename
entry < / source >
<translation>Eintrag
umbenennen < / translation >
< / message >
< / context >
<context>
    <name>TreeListView < / name >
<message>
    <location filename = "tree.py"
line = "388" / >
<source>Confirm
delete< / source >
<translation>Löschen
bestätigen < / translation >
< / message >
<message>
    <location filename = "tree.py"
line = "388" / >
<source>Delete % s
nodes
and % s
leafs ? < / source >
<translation> % s Container
und % s
Einträge
löschen ? < / translation >
< / message >
< / context >
<context>
    <name>TreeSelector < / name >
<message>
    <location filename = "treeselectorUI.py" line = "68" / >
<source>File
selection < / source >
<translation>Dateiauswahl < / translation >
< / message >
<message>
    <location filename = "treeselectorUI.py"
line = "69" / >
<source>Add
to
selected < / source >
<translation>Zur
Auswahl
hinzufügen < / translation >
< / message >
<message>
    <location filename = "treeselectorUI.py"
line = "70" / >
<source>Selected
:
< / source >
<translation>Ausgewählt
:
< / translation >
< / message >
<message>
    <location filename = "treeselectorUI.py"
line = "71" / >
<source>Apply < / source >
<translation>Übernehmen < / translation >
< / message >
< / context >
<context>
    <name>TreeSimpleRenameAction < / name >
<message>
    <location filename = "tree_simple.py"
line = "106" / >
<source>Edit
entry < / source >
<translation>Eintrag
bearbeiten < / translation >
< / message >
<message>
    <location filename = "tree_simple.py"
line = "113" / >
<source>Rename
directory < / source >
<translation>Verzeichnis
umbenennen < / translation >
< / message >
<message>
    <location filename = "tree_simple.py"
line = "113" / >
<source>New
name:< / source >
<translation>Neuer
Name:< / translation >
< / message >
< / context >
<context>
    <name>TreeWidget < / name >
<message>
    <location filename = "tree.py"
line = "527" / >
<source>Edit
entry < / source >
<translation>Eintrag
bearbeiten < / translation >
< / message >
<message>
    <location filename = "tree.py"
line = "344" / >
<source>Cut < / source >
<translation>Ausschneiden < / translation >
< / message >
<message>
    <location filename = "tree.py"
line = "346" / >
<source>Delete < / source >
<translation>Löschen < / translation >
< / message >
<message>
    <location filename = "tree.py"
line = "349" / >
<source>Insert < / source >
<translation>Einfügen < / translation >
< / message >
<message>
    <location filename = "tree.py"
line = "638" / >
<source>Moving
: %
s
of % s
finished. < / source >
<translation>Verschiebe
: %
s
von % s
verschoben < / translation >
< / message >
<message>
    <location filename = "tree.py"
line = "640" / >
<source>Copying
: %
s
of % s
finished. < / source >
<translation>Kopiere
: %
s
von % s
kopiert. < / translation >
< / message >
<message>
    <location filename = "tree.py"
line = "642" / >
<source>Deleting
: %
s
of % s
removed. < / source >
<translation>Lösche
: %
s
von % s
gelöscht < / translation >
< / message >
< / context >
<context>
    <name>Updater < / name >
<message>
    <location filename = "updaterUI.py"
line = "81" / >
<source>ViUR
Admin
Updater < / source >
<translation>ViUR
Admin
Updater < / translation >
< / message >
<message>
    <location filename = "updaterUI.py"
line = "82" / >
<source>Check
for Updates < / source >
    <translation>Nach Updates
suchen < / translation >
< / message >
<message>
    <location filename = "updaterUI.py"
line = "83" / >
<source>Update
now < / source >
<translation>Update
starten < / translation >
< / message >
<message>
    <location filename = "updater.py"
line = "88" / >
<source>Searching
for updates < / source >
    <translation>Suche nach
Updates < / translation >
< / message >
<message>
    <location filename = "updater.py"
line = "112" / >
<source>Updates
avaiable.Ready
to
upgrade. < / source >
<translation>Update
verfügbar.Bereit
zum
updaten. < / translation >
< / message >
<message>
    <location filename = "updater.py"
line = "115" / >
<source>There
is
no
new version
avaiable. < / source >
<translation>Es
ist
keine
neue
Version
verfügbar. < / translation >
< / message >
<message>
    <location filename = "updater.py"
line = "133" / >
<source>Securityviolation
inside
the
update
!!!Update
aborted
!< / source >
<translation>Sicherheitsverletzung
innerhalb
des
Updates
!Aktualisierung
abgebrochen
!< / translation >
< / message >
<message>
    <location filename = "updater.py"
line = "135" / >
<source>Extracting
: %
s < / source >
<translation>Extrahieren
: %
s < / translation >
< / message >
<message>
    <location filename = "updater.py"
line = "161" / >
<source>Update
successful < / source >
<translation>Aktualisierung
erfolgreich < / translation >
< / message >
<message>
    <location filename = "updaterUI.py"
line = "84" / >
<source>Exit < / source >
<translation>Schließen < / translation >
< / message >
<message>
    <location filename = "network.py"
line = "457" / >
<source>The
cacerts.pem
file
is
missing
or
invalid.Your
passwords
and
data
will
be
send
unsecured
!Continue
without
encryption ? If unsure, choose & quot;
abort & quot;
!< / source >
<translation>Die
Datei
cacerts.pem
fehlt
oder
ist
ungültig.Ihre
Daten
und
Passwörter
werden
unverschlüsselt
übertragen.Fortfahren ? < / translation >
< / message >
< / context >
<context>
    <name>rawTextEditWindow < / name >
<message>
    <location filename = "rawtexteditUI.py" line = "44" / >
<source>Apply < / source >
<translation>Übernehmen < / translation >
< / message >
<message>
    <location filename = "rawtexteditUI.py"
line = "43" / >
<source>Form < / source >
<translation>Form < / translation >
< / message >
< / context >
<context>
    <name>relationalSelector < / name >
<message>
    <location filename = "relationalselectionUI.py"
line = "79" / >
<source>Form < / source >
<translation>Form < / translation >
< / message >
<message>
    <location filename = "relationalselectionUI.py"
line = "80" / >
<source>Selected
:
< / source >
<translation>Ausgewählt
:
< / translation >
< / message >
<message>
    <location filename = "relationalselectionUI.py"
line = "81" / >
<source>Abort < / source >
<translation>Abbrechen < / translation >
< / message >
<message>
    <location filename = "relationalselectionUI.py"
line = "82" / >
<source>Apply < / source >
<translation>Übernehmen < / translation >
< / message >
< / context >
<context>
    <name>tasks < / name >
<message>
    <location filename = "tasks.py"
line = "10" / >
<source>Tasks < / source >
<translation>Aufgaben < / translation >
< / message >
< / context >
<context>
    <name>textEditWindow < / name >
<message>
    <location filename = "texteditUI.py"
line = "56" / >
<source>MainWindow < / source >
<translation>Hauptfenster < / translation >
< / message >
<message>
    <location filename = "texteditUI.py"
line = "57" / >
<source>Apply < / source >
<translation>Übernehmen < / translation >
< / message >
< / context >
< / TS >
